package com.barneys.giftregistry.utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.io.*;

import org.apache.log4j.Logger;

import com.barneys.giftregistry.MainClass;
import com.barneys.giftregistry.utility.*;

public class FileHandling {
	
	static final Logger LOGGER = Logger.getLogger(FileHandling.class.getName());
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy_MM_dd");
	Date now = new Date();
	
	public void getFiles(String inputFolderString) {
		List<String> bnyfiles = new ArrayList<String>();
		List<String> whsfiles = new ArrayList<String>();
		FileFilterClass textFileFilter = new FileFilterClass(".txt");
		File inputFolder = new File(inputFolderString);
		File[] listOfFiles = inputFolder.listFiles(textFileFilter);
		if(listOfFiles.length == 0){
			LOGGER.info("No text files available for conversion.");
			//System.exit(0);
		} else {
			for (File file : listOfFiles) {
				if(file.getName().contains("www.barneys.com")) {
					bnyfiles.add(file.getAbsolutePath());
				}
				else if(file.getName().contains("www.barneyswarehouse.com")) {
					whsfiles.add(file.getAbsolutePath());
				}
			}
			zipFiles(inputFolderString,bnyfiles,"BNY");
			zipFiles(inputFolderString,whsfiles,"WHS");
		}
		
		
	}
	
	public void zipFiles(String outputFolder,List<String> files, String site){
        String siteUrl;
        
        if(site.equals("BNY")) {
        	siteUrl = "www.barneys.com";
        }
        else {
        	siteUrl = "www.barneyswarehouse.com";
        }
        
        FileOutputStream fos = null;
        ZipOutputStream zipOut = null;
        FileInputStream fis = null;
        try {
            fos = new FileOutputStream(outputFolder + File.separator +"catalog_" + siteUrl + "_" + dateFormatter.format(now)+".zip");
            zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
            for(String filePath:files){
                File input = new File(filePath);
                fis = new FileInputStream(input);
                ZipEntry ze = new ZipEntry(input.getName());
                LOGGER.info("Zipping the file: "+input.getName());
                zipOut.putNextEntry(ze);
                byte[] tmp = new byte[4*1024];
                int size = 0;
                while((size = fis.read(tmp)) != -1){
                    zipOut.write(tmp, 0, size);
                }
                zipOut.flush();
                fis.close();
            }
            zipOut.close();
            LOGGER.info("Zipped the files");
            deleteFiles(files);
        } catch (Exception e) {
            LOGGER.error("Error while zipping files");
            e.printStackTrace();
       
        } finally{
            try{
                if(fos != null) fos.close();
            } catch(Exception ex){
                 
            }
        }
    }
	
	public void zipDir(String zipFileName, String dir) throws Exception {
	    File dirObj = new File(dir);
	    ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
	    //System.out.println("Creating : " + zipFileName);
	    addDir(dirObj, out,"Directory");
	    addDir(new File(dirObj.getParent()+File.separator+"ThirdPartyFeedReport.xlsx"),out,"File");
	    out.close();
	  }

	public void addDir(File dirObj, ZipOutputStream out, String type) throws IOException {
		byte[] tmpBuf = new byte[1024];
		if(type.equals("File")) {
			FileInputStream in = new FileInputStream(dirObj.getAbsolutePath());
		      //System.out.println(" Adding: " + files[i].getParentFile().getName()+File.separator+files[i].getName());
		      out.putNextEntry(new ZipEntry(dirObj.getName()));
		      int len;
		      while ((len = in.read(tmpBuf)) > 0) {
		        out.write(tmpBuf, 0, len);
		      }
		      out.closeEntry();
		      in.close();
//		      deleteFiles(dirObj);
		}
		else {
		File[] files = dirObj.listFiles();
	    for (int i = 0; i < files.length; i++) {
	      if (files[i].isDirectory()) {
	    	 //System.out.println(files[i].getName());
	    	 
	    		 addDir(files[i], out, "Directory");
	    		 continue;
	    	 
	      }
	      FileInputStream in = new FileInputStream(files[i].getAbsolutePath());
	      //System.out.println(" Adding: " + files[i].getParentFile().getName()+File.separator+files[i].getName());
	      out.putNextEntry(new ZipEntry(files[i].getParentFile().getName()+File.separator+files[i].getName()));
	      int len;
	      while ((len = in.read(tmpBuf)) > 0) {
	        out.write(tmpBuf, 0, len);
	      }
	      out.closeEntry();
	      in.close();
	    }
	    deleteFiles(dirObj);
		}
	   
	        
	  }
	
	public void deleteFiles(List<String> files) {
		
		for(String filePath:files){
			 File file = new File(filePath);
			 if (!file.delete()) {
			      file.deleteOnExit();
			    }
		}
		
	}
	
	public void deleteFiles(File files) {
	
//	 if(files.isFile()) {
//		 files.delete();
//	 }
//	 else {
	 for(File f : files.listFiles()) {
		    if (f.isDirectory()) {
		        for (File c : f.listFiles())
		          c.delete();
		      }
		      if (!f.delete())
		        LOGGER.info("Failed to delete file: " + f.getAbsolutePath());
		    }
	 }
//	}
	
//	public static void main(String args[]) {
//		FileHandling obj = new FileHandling();
//		obj.getFiles("C:\\ReportGeneration\\Output");
//	}
	
}
