
package com.barneys.giftregistry.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import org.apache.log4j.Logger;

public class FileTransfer {

	String outputFolder;

	static final Logger LOGGER = Logger.getLogger(FileTransfer.class.getName());

	public void transferFiles() throws IOException {
		Properties prop = new Properties();
		InputStream input = new FileInputStream("ReportGeneration.properties");
		prop.load(input);

		String giftRegistryUat = prop.getProperty("GiftRegistryUatFTP");
		String giftRegistryProd = prop.getProperty("GiftRegistryProdFTP");
		String transferFlag = prop.getProperty("FTPServer");
		String ftpDetails = "";
		if (transferFlag.equalsIgnoreCase("uat")) {
			copyFilesToFTP(outputFolder + File.separator + "GiftRegistryReport", giftRegistryUat, "None");
		} else if (transferFlag.equalsIgnoreCase("prod")) {
			copyFilesToFTP(outputFolder + File.separator + "GiftRegistryReport", giftRegistryProd, "None");
		} else if (transferFlag.equalsIgnoreCase("both")) {
			copyFilesToFTP(outputFolder + File.separator + "GiftRegistryReport", giftRegistryUat, "Both");
			copyFilesToFTP(outputFolder + File.separator + "GiftRegistryReport", giftRegistryProd, "None");
		}
		else {
			copyFilesToFTP(outputFolder + File.separator + "GiftRegistryReport", giftRegistryUat, "None");
		}

	}

	public FileTransfer(String outputFolder) {
		this.outputFolder = outputFolder;
	}

	public void moveFileToAnotherFolder(String file, String folder) {

		InputStream inStream = null;
		OutputStream outStream = null;

		try {

			File afile = new File(file);
			File bfile = new File(folder + File.separator + afile.getName());
			// if(!bfile.exists()) {
			// bfile.mkdir();
			// }
			inStream = new FileInputStream(afile);
			outStream = new FileOutputStream(bfile);
			byte[] buffer = new byte[1024];
			int length;
			// copy the file content in bytes
			while ((length = inStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, length);
			}
			inStream.close();
			outStream.close();
			// delete the original file
			afile.getAbsolutePath();
			afile.delete();
			// LOGGER.info("Archived !!!");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void copyFilesToFTP(String inputFolder, String ftpDetails, String flag) {

		Boolean isLinkshare = false, toContinue = false;
		String SFTPWORKINGDIR = "", SFTPHOST = "", SFTPUSER = "", SFTPPASS = "";
		int SFTPPORT = 22;
		// System.out.println("Entering copyfiles");
		SFTPHOST = ftpDetails.split(",")[0];
		SFTPPORT = 22;
		SFTPUSER = ftpDetails.split(",")[1];
		SFTPPASS = ftpDetails.split(",")[2];
		if (ftpDetails.split(",").length == 4) {
			SFTPWORKINGDIR = ftpDetails.split(",")[3];
		}

		String folderArr = new File(inputFolder).getName();

		String archiveFolder = this.outputFolder + File.separator + "Archive" + File.separator + folderArr;
		System.out.println(archiveFolder);

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
			session.setPassword(SFTPPASS);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);

			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			if (ftpDetails.split(",").length == 4) {
				// LOGGER.info("Change directory : " + SFTPWORKINGDIR);
				channelSftp.cd(SFTPWORKINGDIR);
			}
			File file = new File(inputFolder);
			if (!file.exists()) {
				LOGGER.info(inputFolder + " doesnot exist !!!");
			} else {
				File[] files = file.listFiles();

				if (files != null) {
					for (int i = 0; i < files.length; i++) {
						if (files[i].isFile() == true) {
							try {

								LOGGER.info("File Name : " + files[i].getName());
								File newFile = new File(files[i].getAbsolutePath());
								String outputFile = newFile.getAbsolutePath();
								File firstLocalFile = new File(outputFile);
								String firstRemoteFile = firstLocalFile.getName();
								InputStream inputStream = new FileInputStream(firstLocalFile);
								channelSftp.put(inputStream, firstRemoteFile);
								inputStream.close();

								if (!flag.equals("Both")) {
									if (new File(archiveFolder).exists()) {
										// Moving To Archive Folder
										moveFileToAnotherFolder(newFile.getAbsolutePath(), archiveFolder);
									} else {
										new File(archiveFolder).mkdir();
										moveFileToAnotherFolder(newFile.getAbsolutePath(), archiveFolder);
										// LOGGER.info("No archive folder");
									}
								}

							} catch (FileNotFoundException e) {
								e.printStackTrace();
								continue;
							}

						}
					}
				} else {
					LOGGER.info("No Files Found");
				}

			}
			LOGGER.info("Files Transferred from " + inputFolder + " & archived at " + archiveFolder);
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (session.isConnected()) {
				channelSftp.exit();
				channel.disconnect();
				session.disconnect();
			}

		}
	}
}
