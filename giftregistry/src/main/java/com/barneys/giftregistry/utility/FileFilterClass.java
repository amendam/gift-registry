package com.barneys.giftregistry.utility;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.zip.ZipFile;
import java.io.FileInputStream;

public class FileFilterClass implements FileFilter {
	
	private String fileExtension;
	private String fileName;
	
	public FileFilterClass(String fex) {
		this.fileExtension = fex;
		this.fileName = null;
	}

	public FileFilterClass(String fex, String name) {
		this.fileExtension = fex.toLowerCase();
		this.fileName = name;
	}

	public boolean accept(File file) {

		if (file.getName().toLowerCase().endsWith(fileExtension) && ifu(file)) {
			if(this.fileName == null || this.fileName.isEmpty()) {
				return true;
			}
			else {
				if(file.getName().contains(this.fileName))
					return true;
				else
					return false;
			}
		} else {
			return false;
		}

	}
	
	public static boolean ifu(File file) {
		try {
			FileInputStream in = new FileInputStream(file);
			if (in!=null) {
				in.close();
			}
			return true;
		} catch (FileNotFoundException e) {
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
