package com.barneys.giftregistry;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.barneys.giftregistry.POJO.Classification;
import com.barneys.giftregistry.POJO.SKU;
import com.barneys.giftregistry.POJO.Style;
import com.barneys.giftregistry.POJO.SuperStyle;

public class XmlParser extends DefaultHandler {
	
	static final Logger LOGGER = Logger.getLogger(XmlParser.class.getName());
	
	String id,styleId = null,superStyleId = null;
	Boolean isName = false,isMi9 = false, isGiftRegistry = false;
	StringBuilder value = new StringBuilder();
	Boolean valueTag = false;
	Boolean isClassification = false;
	static Boolean rejectFlag = true,bnyCat = false,whsCat = false;
	Classification classificationObj = new Classification();
	Stack<Classification> catStack = new Stack<Classification>();
	List<Classification> catList = new ArrayList<Classification>();
	HashMap<String,Classification> catMap = new HashMap<String,Classification>();
	List<Classification> catListWhs = new ArrayList<Classification>();
	HashMap<String,Classification> catMapWhs = new HashMap<String,Classification>();
	Stack<Classification> giftCatStack = new Stack<Classification>();
	List<Classification> giftCatList = new ArrayList<Classification>();
	HashMap<String,Classification> giftCatMap = new HashMap<String,Classification>();
	Style styleObj = new Style();
	SuperStyle superStyleObj = new SuperStyle();
	SKU skuObj = new SKU();
	Boolean isSuperStyle = false;
	Boolean isStyle = false;
	Boolean isSku = false;
	Boolean isMultiValue = false;
	String tempMultiValue = null;
	List<String> giftRegistryClassificationList = new ArrayList<String>();
	HashMap<String,SuperStyle> superStyleMap = new HashMap<String,SuperStyle>();
	HashMap<String,Style> styleMap = new HashMap<String,Style>();
	HashMap<String,SKU> skuMap = new HashMap<String,SKU>();
	List<String> skuIdList = new ArrayList<String>();
	String attributeName = null;
	HashMap<String,String> mi9Map = new HashMap<String,String>();
	List<HashMap<String,String>> mi9List = new ArrayList<HashMap<String,String>>();
	HashMap<String,String> giftRegMap = new HashMap<String,String>();
	List<HashMap<String,String>> giftRegList = new ArrayList<HashMap<String,String>>();
	
	
	public XmlParser(File xmlInputFile) {
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			Object xmlInput = new FileInputStream(xmlInputFile);
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse((InputStream) xmlInput, this);
		} catch (Throwable err) {
			err.printStackTrace();
		}
	}

	public void startDocument() throws SAXException {
		LOGGER.info("BEGIN parsing XML");
	}

	public void endDocument() throws SAXException {
		LOGGER.info("End of parsing XML");
		
	}

	public void startElement(String uri, String localName, String qName,Attributes attributes) {
		
		if("Classification".equals(qName)) {
			
			id = attributes.getValue("ID");
			if(id.equals("masterCatalog") || id.equals("WHS-masterCatalog")) {
				if(id.equals("masterCatalog")) {
					bnyCat = true;
					whsCat = false;
				}
				else if(id.equals("WHS-masterCatalog")) {
					bnyCat = false;
					whsCat = true;
				}
				rejectFlag = true;
			}
			else if(id.equals("GiftRegistryRoot")) {
				rejectFlag = false;
			}

			if(attributes.getValue("UserTypeID").contains("GiftRegistry")) {
				isGiftRegistry = true;
				rejectFlag = false;
			}
			isClassification = true;
		}
		if("Name".equals(qName)) {
			
		}
		if("Product".equals(qName)) {

			if(attributes.getValue("UserTypeID").equals("Superstyle")) {
					isSuperStyle = true;
					superStyleId = attributes.getValue("ID");
					superStyleObj.setId(superStyleId);
			}
			else if(attributes.getValue("UserTypeID").equals("Style")) {
				isStyle = true;
				styleId = attributes.getValue("ID");
				styleObj.setId(attributes.getValue("ID"));
				
			}
			else if(attributes.getValue("UserTypeID").equals("SKU")) {
				isSku = true;
				skuObj.setId(attributes.getValue("ID"));
				skuIdList.add(attributes.getValue("ID"));
			}
		}
		if("ClassificationReference".equals(qName) && attributes.getValue("Type").equals("StyleToGiftRegistry") ) {
			//System.out.println(attributes.getValue("ClassificationID"));
			giftRegistryClassificationList.add(attributes.getValue("ClassificationID"));
		}
		else if("ClassificationReference".equals(qName)) {
			styleObj.setClassification(attributes.getValue("ClassificationID"));
		}
		
		if("MultiValue".equals(qName)) {
			
			if(attributes.getValue("AttributeID") != null)
			{
				attributeName = attributes.getValue("AttributeID");
			}
			isMultiValue = true;
		}
		if("Value".equals(qName))
		{
			valueTag = true;
			value = new StringBuilder();
			if(attributes.getValue("AttributeID") != null)
			{
				attributeName = attributes.getValue("AttributeID");
			}
		}
	} 

	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if("Name".equals(qName) && isClassification && rejectFlag) {
			classificationObj.setId(id);
			classificationObj.setName(value.toString().trim());
		}

		if("Name".equals(qName) && isClassification && isGiftRegistry) {
			classificationObj.setId(id);
			classificationObj.setName(value.toString().trim());
			giftCatStack.push(classificationObj);
			isClassification = false;
			classificationObj = new Classification();
			//giftRegMap.put(id,value.toString().trim());
			//isGiftRegistry = false;
		}
		if("MetaData".equals(qName)) {
			catStack.push(classificationObj);
			isClassification = false;
			classificationObj = new Classification();
		}
		if("Classification".equals(qName) && isGiftRegistry) {
			//System.out.println("Classification End tag");
			if(!giftCatStack.isEmpty()) {
				Classification tempobj = giftCatStack.pop();
				if(giftCatStack.isEmpty()) {
					tempobj.setParentId("");
				}
				else {
					Classification topobj = giftCatStack.peek();
					if(topobj == null) {
						//check logic
					}
					else {
					if(topobj.getId().equals("GiftRegistryRoot")) {
						tempobj.setParentId("");
					}
					else 
						tempobj.setParentId(topobj.getId());
					}
				}

					giftCatList.add(tempobj);
					giftCatMap.put(tempobj.getId(), tempobj);
				
				
			}
			isGiftRegistry = false;
		}
		if("Classification".equals(qName) && rejectFlag) {
			//System.out.println("Classification End tag");
			if(! catStack.isEmpty()) {
				Classification tempobj = catStack.pop();
				if(catStack.isEmpty()) {
					tempobj.setParentId("");
				}
				else {
					Classification topobj = catStack.peek();
					if(topobj == null) {
						//check logic
					}
					else {
					if(topobj.getId().equals("BNY-RootCategory")||topobj.getId().equals("WHS-RootCategory")) {
						tempobj.setParentId("");
					}
					else 
						tempobj.setParentId(topobj.getId());
					}
				}
				
				if(bnyCat == true && whsCat == false) {
					catList.add(tempobj);
					catMap.put(tempobj.getId(), tempobj);
				}
				else if(whsCat == true && bnyCat == false) {
					catListWhs.add(tempobj);
					catMapWhs.put(tempobj.getId(), tempobj);
				}
				
			}
		}
		if(valueTag) {
			switch(attributeName) {
			case "productname" :
				if(isSuperStyle && !isStyle) // SuperStyle value
					superStyleObj.setProductName(value.toString().trim());
				else if	(isSuperStyle && isStyle) //Style Value
					styleObj.setProductName(value.toString().trim());
					
				break;
			case "mainimage" :
				styleObj.setMainImage(value.toString().trim());
				break;
			case "rawavailabilitystatus" :
				styleObj.setRawAvailabilityStatus(value.toString().trim());
				break;
			case "site":
				if(isStyle) {
					styleObj.setSite(value.toString().trim());
				}
				break;
			case "onlinefromdate":
				if(isClassification == true) {
					classificationObj.setOnlineFromDate(value.toString().trim());
				} else
					styleObj.setOnlineFrom(value.toString().trim());
				break;
			case "onlinetodate" :
				if(isClassification == true) {
					classificationObj.setOnlineToDate(value.toString().trim());
				} else
					styleObj.setOnlineTo(value.toString().trim());
				
				break;
			case "displayname" :
				if(isClassification) {
					classificationObj.setDisplayName(value.toString().trim());
				}
				break;
			case "exclusivelyours" :
				if(isStyle)
					styleObj.setExclusivelyOurs(value.toString().trim());
				else if(isSuperStyle)
					superStyleObj.setExclusivelyOurs(value.toString().trim());
				break;
			case "designer" :
				styleObj.setDesigner(value.toString().trim());;
				break;
			case "isprivatestyle" :
				styleObj.setPrivateStyle(value.toString().trim());
				break;
			case "brandcode" :
				styleObj.setBrandCode(value.toString().trim());
				break;
			case "specialorderflag" :
				styleObj.setSpecialOrderFlag(value.toString().trim());
				break;
			case "giftregistry" :
				styleObj.setGiftRegistry(value.toString().trim());
				break;
			case "realcolors" ://multivalued
				if(tempMultiValue==null)
					tempMultiValue = value.toString().trim();
				else
					tempMultiValue = tempMultiValue+","+value.toString().trim();
				//styleObj.setRealColors(value.toString().trim());
				break;
			case "colors" ://multivalued
				if(tempMultiValue==null)
					tempMultiValue = value.toString().trim();
				else
					tempMultiValue = tempMultiValue+","+value.toString().trim();
				//styleObj.setColors(value.toString().trim());
				break;
			case "materials" :
				if(tempMultiValue==null)
					tempMultiValue = value.toString().trim();
				else
					tempMultiValue = tempMultiValue+","+value.toString().trim();
				//styleObj.setColors(value.toString().trim());
				break;
			case "prints" :
				if(tempMultiValue==null)
					tempMultiValue = value.toString().trim();
				else
					tempMultiValue = tempMultiValue+","+value.toString().trim();
				//styleObj.setColors(value.toString().trim());
				break;
			case "newarrival" :
				styleObj.setNewArrival(value.toString().trim());
				break;
			case "giftregistryfashionstyleflag" :
				styleObj.setGiftregistryfashionstyleflag(value.toString().trim());
				break;
			case "giftregistryfashionstylemessage" :
				styleObj.setGiftregistryfashionstylemessage(value.toString().trim());
				break;
			case "classcode" :
				styleObj.setClasscode(value.toString().trim());
				break;
			case "originalprice" :
				skuObj.setOriginalPrice(value.toString().trim());
				break;
			case "webdescriptiondefault" :
				styleObj.setWebDescriptionDefault(value.toString().trim());
				break;
			case "onorder" :
				skuObj.setOnOrder(value.toString().trim());
				break;
			case "currentretailprice" :
				skuObj.setCurrntRetailPrice(value.toString().trim());
				break;
			case "realsize" :
				skuObj.setSize(value.toString().trim());
				break;
			case "whsonhand" :
				skuObj.setWhsOnHand(value.toString().trim());
			case "onhand" :
				skuObj.setOnHand(value.toString().trim());
				break;
			case "highestretailprice" :
				skuObj.setHighestRetailPrice(value.toString().trim());
				break;
			case "whsonorder" :
				skuObj.setWhsOnOrder(value.toString().trim());
				break;
			case "msrpprice" :
				skuObj.setMsrpPrice(value.toString().trim());
				break;
			case "onsale" :
				skuObj.setOnSale(value.toString().trim());
				break;
			case "gender" :
				classificationObj.setGender(value.toString().trim());
				break;
			case "taxcode":
				styleObj.setTaxCode(value.toString().trim());
				break;
			case "googlecatgname" :
				//System.out.println(classificationObj.getId());
				//System.out.println(value.toString().trim());
				classificationObj.setGoogleCategory(value.toString().trim());
				break;
			case "categorytree" :
				classificationObj.setCategoryTree(value.toString().trim());
				break;
			case "alternateimage" :
				styleObj.setAlternateImage(value.toString().trim());
				break;
			case "romancecopy" :
				if(isStyle)
					styleObj.setRomanceCopy(value.toString().trim());
				else if(isSuperStyle)
					superStyleObj.setRomanceCopy(value.toString().trim());
				break;
			case "bulletcopyfordefinedproduct" :
				if(isStyle)
					styleObj.setBulletCopyForDefinedProduct(value.toString().trim());
				else if(isSuperStyle)
					superStyleObj.setBulletCopyForDefinedProduct(value.toString().trim());
				break;
			case "bulletcopyforopenedproduct" :
				if(isStyle)
					styleObj.setBulletCopyForOpenedProduct(value.toString().trim());
				else if(isSuperStyle)
					superStyleObj.setBulletCopyForOpenedProduct(value.toString().trim());
				break;
			case "copybelowbullets" :
				if(isStyle)
					styleObj.setCopyBelowBullets(value.toString().trim());
				else if(isSuperStyle)
					superStyleObj.setCopyBelowBullets(value.toString().trim());
				break;
			case "standardcopyblurbs" :
				if(tempMultiValue==null)
					tempMultiValue = value.toString().trim();
				else
					tempMultiValue = tempMultiValue+","+value.toString().trim();
				//skuObj.setGtin(value.toString().trim());
				break;
			case "gtin" ://multivalued
				if(tempMultiValue==null)
					tempMultiValue = value.toString().trim();
				else
					tempMultiValue = tempMultiValue+","+value.toString().trim();
				//skuObj.setGtin(value.toString().trim());
				break;
			case "auxiliaryimages" :
				if(tempMultiValue==null)
					tempMultiValue = value.toString().trim();
				else
					tempMultiValue = tempMultiValue+","+value.toString().trim();
				//skuObj.setGtin(value.toString().trim());
				break;
			
			default :
				break;
			}
		} 
		if("MultiValue".equals(qName)) {
			isMultiValue = false;
			
			if(attributeName.equals("gtin"))
				skuObj.setGtin(tempMultiValue);
			else if (attributeName.equals("realcolors"))
				styleObj.setRealColors(tempMultiValue);
			else if(attributeName.equals("colors"))
				styleObj.setColors(tempMultiValue);
			else if(attributeName.equals("materials"))
				styleObj.setMaterials(tempMultiValue);
			else if(attributeName.equals("prints"))
				styleObj.setPrints(tempMultiValue);
			else if(attributeName.equals("auxiliaryimages"))
				styleObj.setAuxillaryImages(tempMultiValue);
			else if(attributeName.equals("standardcopyblurbs")) {
				if(isStyle) {
					styleObj.setStandardCopyBlurbs(tempMultiValue);
				}
				else if(isSuperStyle) {
					superStyleObj.setStandardCopyBlurbs(tempMultiValue);
				}
			}
			else if(attributeName.equals("restrictedinternationalcountrycodes")) {
				styleObj.setRestrictedInternationalCountryCodes(tempMultiValue);
			}
			tempMultiValue = null;
			
		}
		if("Product".equals(qName)) {
			if(isStyle == false && isSuperStyle == true) {
				superStyleMap.put(superStyleObj.getId(), superStyleObj);
				isSuperStyle = false;
				superStyleObj = new SuperStyle();
			}
			if(isSku == false && isStyle == true) {
				styleObj.setSkuIdList(skuIdList);
				styleObj.setSuperStyleId(superStyleId);
				styleObj.setGiftRegistryClassification(giftRegistryClassificationList);
				if(styleObj.getClassification().isEmpty()) {
					System.out.println("Classification empty : " + styleObj.getId());
				}
				else {
					styleMap.put(styleObj.getId(), styleObj);
				}
				styleObj = new Style();
				isStyle = false;
				skuIdList = new ArrayList<String>();
				
				//System.out.println(giftRegistryClassificationList.size());
				giftRegistryClassificationList = new ArrayList<String>();
			}
			if(isSku) {
				skuObj.setStyleId(styleId);
				skuObj.setSuperStyleId(superStyleId);
				skuMap.put(skuObj.getId(),skuObj);
				skuObj = new SKU();
				isSku = false;
			}
			
			
		}
		value = null;
		valueTag = false;
		
	
	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {

		if(value == null)
			value = new StringBuilder();
		value.append(ch,start,length);

	}

}
