package com.barneys.giftregistry;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.xml.sax.helpers.DefaultHandler;
import javax.mail.*;

import com.barneys.giftregistry.POJO.Classification;
import com.barneys.giftregistry.POJO.SKU;
import com.barneys.giftregistry.POJO.Style;
import com.barneys.giftregistry.POJO.SuperStyle;
import com.barneys.giftregistry.utility.FileFilterClass;
import com.barneys.giftregistry.utility.FileHandling;
import com.barneys.giftregistry.utility.FileTransfer;

public class MainClass {

	static final Logger LOGGER = Logger.getLogger(MainClass.class.getName());
	
	static List<Classification> catList = new ArrayList<Classification>();
	static List<Classification> catListWhs = new ArrayList<Classification>();
	static HashMap<String,Classification> catMap = new HashMap<String,Classification>();
	static HashMap<String,Classification> catMapWhs = new HashMap<String,Classification>();
	static HashMap<String,Classification> giftRegMap = new HashMap<String,Classification>();
	static HashMap<String,String> mi9Map = new HashMap<String,String>();
	static HashMap<String,SuperStyle> superStyleMap = new HashMap<String,SuperStyle>();
	static HashMap<String,Style> styleMap = new HashMap<String,Style>();
	static HashMap<String,SKU> skuMap = new HashMap<String,SKU>();	
	

	public static void  main(String args[]) {
	    //BasicConfigurator.configure();
		Boolean renameStatus;
		try {
			
//			Getting input data
			Properties prop = new Properties();
			InputStream input = new FileInputStream("ReportGeneration.properties");
			prop.load(input);
			String inputFolderString = prop.getProperty("InputFolder");
			String outputFolderString = prop.getProperty("OutputFolder");
	
			//Check Input Folder
			File inputFolder = new File(inputFolderString);
			if(!inputFolder.exists()) {
				LOGGER.error("Invalid Input Folder Location - Job aborted" );
				System.exit(0);
			}
			//Check output folder
			File outputfolder = new File(outputFolderString);
			if (!outputfolder.exists()) {
				LOGGER.error("Invalid Output Folder Location - Job aborted" );
				System.exit(0);
			}
			//Check and create archive folder if needed
			File inputArchiveFolder = new File(prop.getProperty("InputArchiveFolder"));
			if(!inputArchiveFolder.exists()) {
				inputArchiveFolder.mkdir();
			}	
			
			File outputArchiveFolder = new File(outputFolderString+File.separator+"Archive");
			if(!outputArchiveFolder.exists()) {
				outputArchiveFolder.mkdir();
			}
			LOGGER.info("Job GiftRegistry Feed - Started");
			
			//Filtering XMLs
			FileFilterClass xmlFileFilter = new FileFilterClass(".xml","GiftRegistry");
			File[] xmlFileList = inputFolder.listFiles(xmlFileFilter);
			if(xmlFileList.length == 0){
				LOGGER.error("No XML files available for conversion.");
				System.exit(0);
			} else {
				LOGGER.info("Number of XMLs:"+xmlFileList.length);		
				for(File xmlFile : xmlFileList) {
					LOGGER.info("Processing XML:" + xmlFile.getName());
					XmlParser tc = new XmlParser(xmlFile);
					mi9Map.putAll(tc.giftRegMap);
					superStyleMap.putAll(tc.superStyleMap);
					styleMap.putAll(tc.styleMap);
					skuMap.putAll(tc.skuMap);
					catMap.putAll(tc.catMap);
					catMapWhs.putAll(tc.catMapWhs);
					giftRegMap.putAll(tc.giftCatMap);
					
					File archiveFile = new File(inputArchiveFolder + File.separator + xmlFile.getName());
					if(archiveFile.exists()) {
						archiveFile.delete();
					}
					renameStatus = xmlFile.renameTo(archiveFile);
					if (!renameStatus)
						LOGGER.error("Moving xml file to backup folder failed.");
				}
		
				//Report Generation
				LOGGER.info("Feed generation started");
				ReportCreation objrc = new ReportCreation(outputFolderString);
				try {
					objrc.writeProduct(catMap,catMapWhs,mi9Map,skuMap,styleMap,superStyleMap,giftRegMap);	
					LOGGER.info("Feed files got generated");
				} catch (Exception e) {
					LOGGER.error("Report creation failed",e);
					System.exit(0);
				}
				
				FileTransfer filetransferObj = new FileTransfer(outputFolderString);
				LOGGER.info("Started to send out the feeds");
				//filetransferObj = new FileTransfer(outputFolderString);
				try{
					filetransferObj.transferFiles();
					LOGGER.info("All Feeds are sent out");
				} catch (Exception e) {
//					e.printStackTrace();
					LOGGER.error("Transfer of feeds got failed"+GlobalFunctions.getStackTrace(e));
				
				}
				
				}		
			LOGGER.info("Job GiftRegistry Feed - Completed");
			
		}catch(Exception e) {
//			e.printStackTrace();
			LOGGER.error("Job GiftRegistry Feed generation completed with errors.",e);
		}finally {
			
		}


	}

}
