package com.barneys.giftregistry.POJO;

import java.util.ArrayList;
import java.util.List;

public class Style {
	private String id;
	private String classification,productName,webDescriptionDefault,designer,mainImage,alternateImage,materials,prints,
		site,onlineFrom,exclusivelyOurs,colors,realColors,rawAvailabilityStatus,preOrderable,newArrival,classcode,superStyleId,
		romanceCopy,bulletCopyForOpenedProduct,restrictedToGoogle,restrictedToLinkShare,bulletCopyForDefinedProduct, privateStyle,
		copyBelowBullets,standardCopyBlurbs,auxillaryImages,onlineTo,restrictedToBorderFree,restrictedToRichRelevance,
		restrictedInternationalCountryCodes,giftRegistry,brandCode,taxCode,giftregistryfashionstylemessage,giftregistryfashionstyleflag;
	private boolean specialOrderFlag;
	private List<String> skuIdList,giftRegistryClassification;
	
	public Style() {
		id = "";
		superStyleId = "";
		classification = "";
		productName = "";
		webDescriptionDefault = "";
		designer = "";
		mainImage = "";
		privateStyle = "";
		alternateImage = "";
		taxCode = "";
		giftRegistry = "";
		brandCode = "";
		site = "";
		onlineFrom = "";
		onlineTo = "";
		exclusivelyOurs = "";
		colors = "";
		realColors = "";
		rawAvailabilityStatus = "";
		restrictedToGoogle = "";
		restrictedToLinkShare = "";
		preOrderable = "";
		newArrival = "";
		classcode = "";
		romanceCopy = "";
		bulletCopyForOpenedProduct = "";
		bulletCopyForDefinedProduct = "";
		restrictedInternationalCountryCodes = null;
		restrictedToBorderFree = "";
		copyBelowBullets = "";
		standardCopyBlurbs = "";
		materials = "";
		prints = "";
		auxillaryImages = "";
		restrictedToRichRelevance = "";
		giftregistryfashionstyleflag = "";
		giftregistryfashionstylemessage = "";
		skuIdList = new ArrayList<String>();
		giftRegistryClassification = new ArrayList<String>();
		
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return this.id;
	}
	public void setSuperStyleId(String superStyleId) {
		this.superStyleId = superStyleId;
	}
	public String getSuperStyleId() {
		return this.superStyleId;
	}
	public void setGiftregistryfashionstyleflag(String giftregistryfashionstyleflag) {
		this.giftregistryfashionstyleflag = giftregistryfashionstyleflag;
	}
	public String getGiftregistryfashionstyleflag() {
		return this.giftregistryfashionstyleflag;
	}
	public void setGiftregistryfashionstylemessage(String giftregistryfashionstylemessage) {
		this.giftregistryfashionstylemessage = giftregistryfashionstylemessage;
	}
	public String getGiftregistryfashionstylemessage() {
		return this.giftregistryfashionstylemessage;
	}
	public void setSkuIdList(List<String> skuId) {
		this.skuIdList = skuId;
	}
	public List<String> getSkuIdList() {
		return this.skuIdList;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public String getClassification() {
		return this.classification;
	}
	public void setGiftRegistryClassification(List<String> giftRegistryClassification) {
		this.giftRegistryClassification = giftRegistryClassification;
	}
	public List<String> getGiftRegistryClassification() {
		return this.giftRegistryClassification;
	}
	public String getPrivateStyle() {
		return this.privateStyle;
	}
	public void setPrivateStyle(String privateStyle) {
		this.privateStyle = privateStyle;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductName() {
		return this.productName;
	}
	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}
	public String getTaxCode() {
		return this.taxCode;
	}
	public void setWebDescriptionDefault(String webDescriptionDefault) {
		this.webDescriptionDefault = webDescriptionDefault;
	}
	public String getWebDescriptionDefault() {
		return this.webDescriptionDefault;
	}
	public void setrestrictedToRichRelevance(String restrictedToRichRelevance) {
		this.restrictedToRichRelevance = restrictedToRichRelevance;
	}
	public String getrestrictedToRichRelevance() {
		return this.restrictedToRichRelevance;
	}
	public void setRestrictedToGoogle(String restrictedToGoogle) {
		this.restrictedToGoogle = restrictedToGoogle;
	}
	public String getRestrictedToGoogle() {
		return this.restrictedToGoogle;
	}
	public void setRestrictedToLinkShare(String restrictedToLinkShare) {
		this.restrictedToLinkShare = restrictedToLinkShare;
	}
	public String getRestrictedToLinkShare() {
		return this.restrictedToLinkShare;
	}
	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}
	public String getBrandCode() {
		return this.brandCode;
	}
	public void setSpecialOrderFlag(String specialOrderFlag) {
		this.specialOrderFlag = Boolean.valueOf(specialOrderFlag);;
	}
	public boolean getspecialOrderFlag() {
		return this.specialOrderFlag;
	}
	
	public void setGiftRegistry(String giftRegistry) {
		this.giftRegistry = giftRegistry;
	}
	public String getGiftRegistry() {
		return this.giftRegistry;
	}
	
	public void setDesigner(String designer) {
		this.designer = designer;
	}
	public String getDesigner() {
		return this.designer;
	}
	
	public void setMainImage(String alternateImage) {
		this.alternateImage = alternateImage;
	}
	public String getMainImage() {
		return this.alternateImage;
	}
	public void setAlternateImage(String mainImage) {
		this.mainImage = mainImage;
	}
	public String getAlternateImage() {
		return this.mainImage;
	}
	public void setPrints(String prints) {
		this.prints = prints;
	}
	public String getPrints() {
		return this.prints;
	}
	public void setMaterials(String materials) {
		this.materials = materials;
	}
	public String getMaterials() {
		return this.materials;
	}
	
	public void setSite(String site) {
		this.site = site;
	}
	public String getSite() {
		return this.site;
	}
	public void setOnlineFrom(String onlineFrom) {
		this.onlineFrom = onlineFrom;
	}
	public String getOnlineFrom() {
		return this.onlineFrom;
	}
	public void setOnlineTo(String onlineTo) {
		this.onlineTo = onlineTo;
	}
	public String getOnlineTo() {
		return this.onlineTo;
	}
	public void setExclusivelyOurs(String exclusivelyOurs) {
		this.exclusivelyOurs = exclusivelyOurs;
	}
	public String getExclusivelyOurs() {
		return this.exclusivelyOurs;
	}
	public void setColors(String colors) {
		this.colors = colors;
	}
	public String getColors() {
		return this.colors;
	}
	public void setPreOrderable(String preOrderable) {
		this.preOrderable = preOrderable;
	}
	public String getPreOrderable() {
		return this.preOrderable;
	}
	public void setRealColors(String realColors) {
		this.realColors = realColors;
	}
	public String getRealColors() {
		return this.realColors;
	}
	public void setRawAvailabilityStatus(String rawAvailabilityStatus) {
		this.rawAvailabilityStatus = rawAvailabilityStatus;
	}
	public String getRawAvailabilityStatus() {
		return this.rawAvailabilityStatus;
	}
	public void setNewArrival(String newArrival) {
		this.newArrival = newArrival;
	}
	public String getNewArrival() {
		return this.newArrival;
	}
	public void setClasscode(String classcode) {
		this.classcode = classcode;
	}
	public String getClasscode() {
		return this.classcode;
	}
	public void setRomanceCopy(String romanceCopy) {
		this.romanceCopy = romanceCopy;
	}
	public String getRomanceCopy() {
		return this.romanceCopy;
	}
	public void setAuxillaryImages(String auxillaryImages) {
		this.auxillaryImages = auxillaryImages;
	}
	public String getAuxillaryImages() {
		return this.auxillaryImages;
	}
	public void setBulletCopyForOpenedProduct(String bulletCopyForOpenedProduct) {
		this.bulletCopyForOpenedProduct = bulletCopyForOpenedProduct;
	}
	public String getBulletCopyForOpenedProduct() {
		return this.bulletCopyForOpenedProduct;
	}
	public void setBulletCopyForDefinedProduct(String bulletCopyForDefinedProduct) {
		this.bulletCopyForDefinedProduct = bulletCopyForDefinedProduct;
	}
	public String getBulletCopyForDefinedProduct() {
		return this.bulletCopyForDefinedProduct;
	}
	public void setCopyBelowBullets(String copyBelowBullets) {
		this.copyBelowBullets = copyBelowBullets;
	}
	public String getCopyBelowBullets() {
		return this.copyBelowBullets;
	}
	public void setStandardCopyBlurbs(String standardCopyBlurbs) {
		this.standardCopyBlurbs = standardCopyBlurbs;
	}
	public String getStandardCopyBlurbs() {
		return this.standardCopyBlurbs;
	}
	public void setRestrictedToBorderFree(String restrictedToBorderFree) {
		this.restrictedToBorderFree = restrictedToBorderFree;
	}
	public String getRestrictedToBorderFree() {
		return this.restrictedToBorderFree;
	}
	public void setRestrictedInternationalCountryCodes(String restrictedInternationalCountryCodes) {
		this.restrictedInternationalCountryCodes = restrictedInternationalCountryCodes;
	}
	public String getRestrictedInternationalCountryCodes() {
		return this.restrictedInternationalCountryCodes;
	}
	
}
