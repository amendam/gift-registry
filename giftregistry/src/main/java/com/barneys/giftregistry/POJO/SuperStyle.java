package com.barneys.giftregistry.POJO;

public class SuperStyle {
	private String id;
	private String parentId,productName,exclusivelyOurs,romanceCopy,bulletCopyForOpenedProduct,bulletCopyForDefinedProduct,copyBelowBullets,standardCopyBlurbs;
	
	public SuperStyle() {
		id = "";
		parentId = "";
		productName = "";
		exclusivelyOurs = "";
		romanceCopy = "";
		bulletCopyForOpenedProduct = "";
		bulletCopyForDefinedProduct = "";
		copyBelowBullets = "";
		standardCopyBlurbs = "";
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return this.id;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getParentId() {
		return this.parentId;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductName() {
		return this.productName;
	}
	public void setExclusivelyOurs(String exclusivelyOurs) {
		this.exclusivelyOurs = exclusivelyOurs;
	}
	public String getExclusivelyOurs() {
		return this.exclusivelyOurs;
	}
	public void setRomanceCopy(String romanceCopy) {
		this.romanceCopy = romanceCopy;
	}
	public String getRomanceCopy() {
		return romanceCopy;
	}
	
	public void setBulletCopyForOpenedProduct(String bulletCopyForOpenedProduct) {
		this.bulletCopyForOpenedProduct = bulletCopyForOpenedProduct;
	}
	public String getBulletCopyForOpenedProduct() {
		return bulletCopyForOpenedProduct;
	}
	public void setBulletCopyForDefinedProduct(String bulletCopyForDefinedProduct) {
		this.bulletCopyForDefinedProduct = bulletCopyForDefinedProduct;
	}
	public String getBulletCopyForDefinedProduct() {
		return bulletCopyForDefinedProduct;
	}
	public void setCopyBelowBullets(String copyBelowBullets) {
		this.copyBelowBullets = copyBelowBullets;
	}
	public String getCopyBelowBullets() {
		return copyBelowBullets;
	}
	public void setStandardCopyBlurbs(String standardCopyBlurbs) {
		this.standardCopyBlurbs = standardCopyBlurbs;
	}
	public String getStandardCopyBlurbs() {
		return standardCopyBlurbs;
	}
	
}
