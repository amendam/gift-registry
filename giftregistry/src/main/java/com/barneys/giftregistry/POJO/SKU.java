package com.barneys.giftregistry.POJO;

public class SKU {
	private String id,highestRetailPrice,msrpprice,currentRetailPrice,size,onHand,onOrder,whsOnHand,whsOnOrder,
	onSale,gtin,styleId,superStyleId,originalPrice = "";
	public SKU() {
		id = "";
		highestRetailPrice = "";
		msrpprice = "";
		currentRetailPrice = "";
		size = "";
		onHand = "";
		onOrder = "";
		whsOnHand = "";
		whsOnOrder = "";
		onSale = "";
		gtin = "";
		styleId = "";
		superStyleId = "";
		originalPrice = "";
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return this.id;
	}
	public void setSuperStyleId(String superStyleId) {
		this.superStyleId = superStyleId;
	}
	public String getSuperStyleId() {
		return this.superStyleId;
	}
	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}
	public String getStyleId() {
		return this.styleId;
	}
	public void setCurrntRetailPrice(String currentRetailPrice) {
		this.currentRetailPrice = currentRetailPrice;
	}
	public String getCurrentRetailPrice() {
		return this.currentRetailPrice;
	}
	public void setHighestRetailPrice(String highestRetailsPrice) {
		this.highestRetailPrice = highestRetailsPrice;
	}
	public String getHighestRetailPrice() {
		return this.highestRetailPrice;
	}
	public void setOriginalPrice(String originalPrice) {
		this.originalPrice = originalPrice;
	}
	public String getOriginalPrice() {
		return this.originalPrice;
	}
	public void setMsrpPrice(String msrpprice) {
		this.msrpprice = msrpprice;
	}
	public String getMsrpPrice() {
		return this.msrpprice;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getsize() {
		return this.size;
	}
	public void setGtin(String gtin) {
		this.gtin = gtin;
	}
	public String getGtin() {
		return this.gtin;
	}
	public void setOnHand(String onHand) {
		this.onHand = onHand;
	}
	public String getOnHand() {
		return this.onHand;
	}
	public void setOnOrder(String onOrder) {
		this.onOrder = onOrder;
	}
	public String getOnOrder() {
		return this.onOrder;
	}
	public void setWhsOnHand(String whsOnHand) {
		this.whsOnHand = whsOnHand;
	}
	public String getWhsOnHand() {
		return this.whsOnHand;
	}
	public void setWhsOnOrder(String whsOnOrder) {
		this.whsOnOrder = whsOnOrder;
	}
	public String getWhsOnOrder() {
		return this.whsOnOrder;
	}
	public void setOnSale(String onSale) {
		this.onSale = onSale;
	}
	public String getOnSale() {
		return this.onSale;
	}
	
	
	
	
	
}
