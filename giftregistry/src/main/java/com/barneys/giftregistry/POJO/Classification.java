package com.barneys.giftregistry.POJO;

public class Classification {
	private String id;
	private String name;
	private String parentId;
	private String gender,googleCategory,categoryTree,displayName,onlinefromdate,onlinetodate;
	public Classification() {
		id = null;
		name = null;
		parentId = null;
		gender = null;
		googleCategory = null;
		categoryTree = null;
		displayName = null;
		onlinefromdate = null;
		onlinetodate = null;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public void setCategoryTree(String categoryTree) {
		this.categoryTree = categoryTree;
	}
	public void setGoogleCategory(String googleCategory) {
		this.googleCategory = googleCategory;
	}
	public void setOnlineFromDate(String onlineFromDate) {
		this.onlinefromdate = onlineFromDate;
	}
	public void setOnlineToDate(String onlineToDate) {
		this.onlinetodate = onlineToDate;
	}
	public String getOnlineFromDate() {
		return this.onlinefromdate;
	}
	public String getOnlineToDate() {
		return this.onlinetodate;
	}
	
	public String getId() {
		return this.id;
	}
	public String getName() {
		return this.name;
	}
	public String getDisplayName() {
		return this.displayName;
	}
	public String getParentId() {
		return this.parentId;
	}
	public String getGender() {
		return this.gender;
	}
	public String getCategoryTree() {
		return this.categoryTree;
	}
	public String getGoogleCategory() {
		return this.googleCategory;
	}
}