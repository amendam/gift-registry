package com.barneys.giftregistry;

import java.io.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.apache.commons.lang3.StringUtils;

import com.barneys.giftregistry.POJO.Classification;
import com.barneys.giftregistry.POJO.SKU;
import com.barneys.giftregistry.POJO.Style;
import com.barneys.giftregistry.POJO.SuperStyle;

public class ReportCreation {

	static final Logger LOGGER = Logger.getLogger(MainClass.class.getName());
	private Writer giftRegistryWriter;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
	Date now = new Date();

	public HashMap<String, Map<String, String>> feedList = new HashMap<String, Map<String, String>>();

	public void outputDirectoryCreation(String outputFolder) {
		File giftRegistryFolder = new File(outputFolder + File.separator + "GiftRegistryReport");
		if (!giftRegistryFolder.exists()) {
			giftRegistryFolder.mkdir();
		}
	}

	public ReportCreation(String outputFolder) {
		try {
			outputDirectoryCreation(outputFolder);
			(giftRegistryWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFolder
					+ File.separator + "GiftRegistryReport" + File.separator + "giftRegistryBNY_"
					+ dateFormatter.format(now) + ".csv")))).write(
							"id|title|description|product_category|product_type|image_link|additional_image_link|condition|availability|price|brand|item_group_id|color|material|pattern|size|gender|age_group|display_in_registry|designer_id|tax_class|giftregistryfashionstylemessage|giftregistryfashionstyleflag|specialorderflag\n");

		} catch (IOException e) {
			LOGGER.info("[ERROR] : Received unexpected: " + e.getClass() + ": " + e.getStackTrace(), e);
		}

	}

	public void writeProduct(HashMap<String, Classification> catMap, HashMap<String, Classification> catMapWhs,
			HashMap<String, String> mi9Map, HashMap<String, SKU> skuMap, HashMap<String, Style> styleMap,
			HashMap<String, SuperStyle> superStyleMap, HashMap<String, Classification> giftCatMap)
			throws IOException, ParseException {
		iterateProducts(skuMap, styleMap, catMap, catMapWhs, mi9Map, superStyleMap, giftCatMap);
		this.close();
	}

	public void iterateProducts(HashMap<String, SKU> skuMap, HashMap<String, Style> styleMap,
			HashMap<String, Classification> catMap, HashMap<String, Classification> catMapWhs,
			HashMap<String, String> mi9Map, HashMap<String, SuperStyle> superStyleMap,
			HashMap<String, Classification> giftCatMap) throws IOException, ParseException {

		GlobalFunctions funObj = new GlobalFunctions();

		for (HashMap.Entry<String, Style> entry : styleMap.entrySet()) {
			// Local variable for Style & SuperStyle
			String styleId = "", superStyleId = "", gender = "", realColors = "", site = "", productName = "",
					areaId = "", description = "", ssDescription = "", ssRomanceCopy = "", romanceCopy = "", brand = "",
					mainImage = "", newArrival = "", categoryTree = "", ageGroup = "", materials = "", prints = "",
					auxillaryImages = "", availability = "", classificationHierarchy = "", ssCopyBelowBullets = "",
					copyBelowBullets = "", standardCopyBlurbs = "", ssStandardCopyBlurbs = "",
					bulletCopyForOpenedProduct = "", bulletCopyForDefinedProduct = "",
					ssBulletCopyForOpenedProduct = "", ssBulletCopyForDefinedProduct = "", googleCategory = "",
					taxCode = "", giftRegistryClassification = "", giftRegistry = "", brandCode = "", giftregistryfashionstylemessage = "", giftregistryfashionstyleflag = "";
			boolean specialOrderFlag;
			List<String> giftRegistryClassificationList;
			HashMap<String, Classification> categoryMap;
			styleId = entry.getKey();
			System.out.println("Style : " + styleId);
			superStyleId = entry.getValue().getSuperStyleId();
			SuperStyle superStyleObj = superStyleMap.get(superStyleId);
			ssRomanceCopy = superStyleObj.getRomanceCopy();
			ssBulletCopyForOpenedProduct = superStyleObj.getBulletCopyForOpenedProduct();
			ssBulletCopyForDefinedProduct = superStyleObj.getBulletCopyForDefinedProduct();
			ssCopyBelowBullets = superStyleObj.getCopyBelowBullets();
			ssStandardCopyBlurbs = superStyleObj.getStandardCopyBlurbs();
			// Style values

			productName = entry.getValue().getProductName().trim();
			if (productName == "" || productName == " " || productName == null) {
				productName = superStyleObj.getProductName();
			}
			if (productName == "" || productName == " " || productName == null)
				continue;

			realColors = entry.getValue().getRealColors();
			bulletCopyForOpenedProduct = entry.getValue().getBulletCopyForOpenedProduct();
			bulletCopyForDefinedProduct = entry.getValue().getBulletCopyForDefinedProduct();
			copyBelowBullets = entry.getValue().getCopyBelowBullets();
			standardCopyBlurbs = entry.getValue().getStandardCopyBlurbs();

			//availability = "in stock";
			/*
			 * if
			 * (entry.getValue().getRawAvailabilityStatus().equals("PREORDER")||
			 * entry.getValue().getRawAvailabilityStatus().equals("REORDER")) {
			 * availability = "preorder"; } else { availability = "in stock"; }
			 */
			taxCode = entry.getValue().getTaxCode();
			site = entry.getValue().getSite();

			giftRegistry = entry.getValue().getGiftRegistry();
			brandCode = entry.getValue().getBrandCode();

			
			// ********************************************************
			// Classification Values
			/*********************************************************************/
			if (site == null || site.equals("") || site.equals("BNY")) {
				gender = funObj.getValueFromCategory(catMap, entry.getValue().getClassification(), "Gender");
				categoryTree = funObj.getValueFromCategory(catMap, entry.getValue().getClassification(),
						"CategoryTree");
				googleCategory = funObj.getValueFromCategory(catMap, entry.getValue().getClassification(),
						"GoogleCategory");
				classificationHierarchy = funObj.getClassificationHierarchy(entry.getValue().getClassification(),
						catMap);
			} else {
				gender = funObj.getValueFromCategory(catMapWhs, entry.getValue().getClassification(), "Gender");
				categoryTree = funObj.getValueFromCategory(catMapWhs, entry.getValue().getClassification(),
						"CategoryTree");
				googleCategory = funObj.getValueFromCategory(catMapWhs, entry.getValue().getClassification(),
						"GoogleCategory");
				classificationHierarchy = funObj.getClassificationHierarchy(entry.getValue().getClassification(),
						catMapWhs);
			}

			newArrival = entry.getValue().getNewArrival();
			romanceCopy = entry.getValue().getRomanceCopy();
			description = entry.getValue().getWebDescriptionDefault();
			description = funObj.descriptionCalculation(description, romanceCopy, bulletCopyForOpenedProduct,
					bulletCopyForDefinedProduct, copyBelowBullets, standardCopyBlurbs, ssRomanceCopy,
					ssBulletCopyForOpenedProduct, ssBulletCopyForDefinedProduct, ssCopyBelowBullets,
					ssStandardCopyBlurbs);
			brand = entry.getValue().getDesigner();
			giftregistryfashionstyleflag = entry.getValue().getGiftregistryfashionstyleflag();
			giftregistryfashionstylemessage = entry.getValue().getGiftregistryfashionstylemessage();
			giftRegistryClassificationList = entry.getValue().getGiftRegistryClassification();
			if (giftRegistryClassificationList.isEmpty()) {
				giftRegistryClassification = classificationHierarchy;
			} else {
				giftRegistryClassification = funObj.getClassificationHierarchy(giftRegistryClassificationList.get(0),
						giftCatMap);
				for (int i = 1; i < giftRegistryClassificationList.size(); i++) {
					if (!(giftRegistryClassificationList.get(i) == null
							|| giftRegistryClassificationList.get(i).isEmpty()
							|| giftRegistryClassificationList.get(i).equals("")))
						giftRegistryClassification = ","
								+ funObj.getClassificationHierarchy(giftRegistryClassificationList.get(i), giftCatMap);
				}

				giftRegistryClassification = giftRegistryClassification + ";" + classificationHierarchy;
			}

			mainImage = entry.getValue().getMainImage();
			auxillaryImages = entry.getValue().getAuxillaryImages();
			materials = entry.getValue().getMaterials();
			prints = entry.getValue().getPrints();
			ageGroup = funObj.calculateAgeGroup(areaId);

			// Iterating SKU's under Style
			for (String tempSku : entry.getValue().getSkuIdList()) {
				System.out.println("Inside sku : " + tempSku);
				// Local variables for SKU
				String skuId = "";
				String size = "", currentRetailPrice = "";

				SKU tempSkuObj = skuMap.get(tempSku);
				skuId = tempSku;
				size = tempSkuObj.getsize();
				currentRetailPrice = tempSkuObj.getCurrentRetailPrice();
				
				//specialOrderFlag//
				specialOrderFlag = entry.getValue().getspecialOrderFlag();
				availability = "in stock";

				if (StringUtils.isBlank(currentRetailPrice)) {
					LOGGER.info("No currentretailprice for the Sku : " + skuId);
				}
				else {
					giftRegistryWriter.write(skuId + "|" + productName + "|" + description + "|"
						+ giftRegistryClassification + "|" + " " + "|" + mainImage + "|" + auxillaryImages + "|"
						+ newArrival + "|" + "in stock" + "|" + currentRetailPrice + "|" + brand + "|" + styleId + "|"
						+ realColors + "|" + materials + "|" + prints + "|" + size + "|" + gender + "|" + ageGroup + "|"
						+ giftRegistry + "|" + brandCode + "|" + taxCode + "|" + giftregistryfashionstylemessage + "|" 
						+ giftregistryfashionstyleflag + "|" + specialOrderFlag + "\n");
				}

			}
		}
	}

	// Count Report - Can be added for overall summary

	public void close() throws IOException {
		this.giftRegistryWriter.close();
	}

}
