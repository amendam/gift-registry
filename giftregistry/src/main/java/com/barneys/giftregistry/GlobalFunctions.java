package com.barneys.giftregistry;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.barneys.giftregistry.POJO.Classification;

public class GlobalFunctions {

	public String getValueFromCategory(HashMap<String,Classification> catMap,
			String id, String name) {
		String str = "";
		
		Classification tempobj = catMap.get(id);
		if(tempobj == null) {
			System.out.println("NULL Classification Id : " + id);
		}
		//System.out.println("Inside global : " + tempobj.getGoogleCategory());
		if(tempobj!=null) {
//		for (Classifiaction_POJO tempobj : catList) {
			if (tempobj.getId().equals(id)) {
				if (name.equals("Gender")) {
					str = tempobj.getGender();
					if (str == null || str.isEmpty() || str.equals("NA")) {
						str = "Unisex";
					}
				} else if (name.equals("GoogleCategory")) {
					str = tempobj.getGoogleCategory();
					if (str == null || str.isEmpty() || str.equals("")) {
						str = "Apparel & Accessories";
					}
					str = str.replaceAll("&amp;","&");
					str = str.replaceAll("&gt;", ">");
				}
				else if (name.equals("CategoryTree")) {
					str = tempobj.getCategoryTree();
					if (str == null || str.equals("") || str.isEmpty()) {
						str = "womens";
					}
					str = str.replaceAll("-", " > ");
				}
			}
		}
//		}
		return str;
	}

	public static String getStackTrace(Exception e)
	{
	    StringWriter sWriter = new StringWriter();
	    PrintWriter pWriter = new PrintWriter(sWriter);
	    e.printStackTrace(pWriter);
	    return sWriter.toString();
	}
	
	public String auxillaryImagesCalculate(String auxillaryImages, String mainImage) {
		String tempImages = "";
		String returnImage = "";
		
		
		if(!(auxillaryImages.equals("") || auxillaryImages == null)) {
		String temp[] = auxillaryImages.split(",");
		for(String tempImg : temp) {
			if(!(tempImg==null || tempImg.equals("")) && !(tempImg.equals(mainImage))) {
				if(tempImages.equals("")) {
					tempImages = tempImg;
				}
				else {
					tempImages = tempImages+","+tempImg;
				}
			}
		}
		}
		
		returnImage = tempImages;
		return returnImage;
	}
	
	public String calculateOnlineFlag(String onlineFromDate, String onlineToDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(onlineFromDate.equals("") || onlineFromDate == null ||onlineToDate.equals("") || onlineToDate == null) {
			return "false";
		}
		Date startDate = df.parse(onlineFromDate);
		Date endDate = df.parse(onlineToDate);
		Date currentDate = new Date();
		
		
		if((currentDate.after(startDate)||currentDate.equals(startDate)) && (currentDate.before(endDate)||currentDate.equals(endDate)))
			return "true";
		else
			return "false";
	}
	
	public String getClassificationHierarchy(String classificationId, HashMap<String,Classification> catMap) {
		List<String> classificationHierarchy = new ArrayList<String>();
		classificationHierarchy.add(catMap.get(classificationId).getName());
		String classificationParentId = catMap.get(classificationId).getParentId();
		
		while (!(classificationParentId==null || classificationParentId.isEmpty() || classificationParentId.equals("")) ) {
			String classificationParentName = catMap.get(classificationParentId).getName();
			//System.out.println(classificationParentName);
			classificationHierarchy.add(classificationParentName);
			classificationParentId = catMap.get(classificationParentId).getParentId();
		}
		String finalClassificationHierarchy = "";
		//System.out.println("Size : " + classificationHierarchy.size());
		if(classificationHierarchy.size() > 0) {
			finalClassificationHierarchy = classificationHierarchy.get(classificationHierarchy.size()-1);
		}
		for (int i=classificationHierarchy.size()-2;i>=0;i--) {
			finalClassificationHierarchy = finalClassificationHierarchy + " > " + classificationHierarchy.get(i);
			
		}
		return finalClassificationHierarchy;
	}

	
	public String calculateAgeGroup(String areaCode) {
		if(areaCode!=null||!areaCode.isEmpty()) {
			if(areaCode.equals("81")) {
				return "Kids";
			}
			else {
				return "Adult";
			}
		}
		else {
			return "Adult";
		}		
	}

	
	public String descriptionCalculation(String webDescription, String romanceCopy, String bulletCopyForOpenedProduct, String bulletCopyForDefinedProduct, String copyBelowBullets, String standardCopyBlurbs, String ssRomanceCopy, String ssBulletCopyForOpenedProduct, String ssBulletCopyForDefinedProduct, String ssCopyBelowBullets, String ssStandardCopyBlurbs) {

		String desc = "";
		if (!romanceCopy.equals("")) {
//			System.out.println("RC Present");
			desc = romanceCopy;
		} else {
			if (!ssRomanceCopy.equals("")) {
//				System.out.println("RC Present");
				desc = ssRomanceCopy;
			}
		}	
		if (!bulletCopyForOpenedProduct.equals("")) {
//			System.out.println("bulletCopyForOpenedProduct Present");
			if (!desc.isEmpty()) {
				
				desc += " "+bulletCopyForOpenedProduct;
			} else {
				
				desc = bulletCopyForOpenedProduct;
			}
		} else {
			if (!ssBulletCopyForOpenedProduct.equals("")) {
//				System.out.println("ssBulletCopyForOpenedProduct Present");
				if (!desc.isEmpty()) {
					desc += " "+ssBulletCopyForOpenedProduct;
					
				} else {
					desc = ssBulletCopyForOpenedProduct;
				}
			}
		}
		if (!bulletCopyForDefinedProduct.equals("")) {
//			System.out.println("bulletCopyForDefinedProduct Present");
			if (!desc.isEmpty()) {
				desc += " "+bulletCopyForDefinedProduct;
			} else {
				desc = bulletCopyForDefinedProduct;
			}
		} else {
			if (!ssBulletCopyForDefinedProduct.equals("")) {
//				System.out.println("ssBulletCopyForDefinedProduct Present");
				if (!desc.isEmpty()) {
					desc += " "+ssBulletCopyForDefinedProduct;
				} else {
					desc = ssBulletCopyForDefinedProduct;
				}
			}
		}
		if (!copyBelowBullets.equals("")) {
//			System.out.println("copyBelowBullets Present");
			if (!desc.isEmpty()) {
				desc += " "+copyBelowBullets;
			} else {
				desc = copyBelowBullets;
			}
		} else {
			if (!ssCopyBelowBullets.equals("")) {
//				System.out.println("copyBelowBullets Present");
				if (!desc.isEmpty()) {
					desc += " "+ssCopyBelowBullets;
				} else {
					desc = ssCopyBelowBullets;
				}
			}
		}
		if (!standardCopyBlurbs.equals("")) {
//			System.out.println("standardCopyBlurbs Present");
			if (!desc.isEmpty()) {
				desc += " "+standardCopyBlurbs;
			} else {
				desc = standardCopyBlurbs;
			}
		} else {
			if (!ssStandardCopyBlurbs.equals("")) {
//				System.out.println("standardCopyBlurbs Present");
				if (!desc.isEmpty()) {
					desc += " "+ssStandardCopyBlurbs;
				} else {
					desc = ssStandardCopyBlurbs;
				}
			}
		}
		if (desc.isEmpty()) {
			desc = webDescription;
		}
		
		//String t = stripHtml(desc);
		String t = desc.replace("&lt;","<").replace("&gt;",">").replace("<lt/>","<").replace("<gt/>",">").replace("\n", "").replace("\r", "").replace("\t", "");
		if(t.contains("<")) {
//			System.out.println("Web Description : " + webDescription);
//			System.out.println("After removing HTML : " + t);
		}
		
		//t = encodeHTML(desc);
		
		return t.trim();
	}
	public String stripHtml(String str) {
		str = str.replaceAll("\\<.*?>", " ").replaceAll("\\s+", " ");
		str = str.replaceAll("&nbsp;", " ");
		return str;
	}
	
	public static String encodeHTML(String s)
	{
	    StringBuffer out = new StringBuffer();
	    for(int i=0; i<s.length(); i++)
	    {
	        char c = s.charAt(i);
	        if(c > 127 || c=='"' || c=='<' || c=='>')
	        {
	           out.append("&#"+(int)c+";");
	        }
	        else
	        {
	            out.append(c);
	        }
	    }
	    return out.toString();
	}
	
}
